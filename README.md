# ReadMe
Lily at Lily's 製 U#版 PAPI（滑走路進入角指示灯）のReadmeです。  
以下、使用方法等について記載します。  

# 主要構成品
PAPI_at_Udon_by_Lily_v1.1.unitypackage  
PAPI  
├ PAPI.unity : PAPIテストシーン  
├ Prefabs  
│ ├ PAPI.prefab : PAPI本体Prefab  
│ └ Indicator.pregab : 指示灯オブジェクトPrefab  
└ PAPI_UdonProgramSources  
　 ├ PAPIController.cs : PAPI本体 U#ソースコード  
　 └ IndicatorController.cs : 指示灯オブジェクト U#ソースコード

# 開発環境（動作確認環境）
- Unity 2018.4.20f1
- VRCSDK3-WORLD-2020.10.28.15.57_Public
- UdonSharp_v0.18.7
- PostProcessing v2.3.0

# 使い方
## Installation
VRChat公式からVRCSDK3-WORLDのunitypackage、  
及び、UdonSharpのunitypackageを以下githubからダウンロードしておいてください。  
[UdonSharp](https://github.com/MerlinVR/UdonSharp/releases, "https://github.com/MerlinVR/UdonSharp/releases")  
  
Unityで新規プロジェクト、または既存のVRChatワールドプロジェクトを開いてください。  
  
Assets->Import Package->Custom Packageから  
・VRCSDK3  
・UdonSharp  
をインポートしてください。Assets/UdonSharp/Examplesディレクトリは削除する必要があるかもしれません。  
Window->Package Managerから  
・PostProcessing Version 2.3.0  
をインストールしてください。  
上記3点は、既にインストール済みの場合、作業不要です。
  
その後、Assets->Import Package->Custom Packageから  
・PAPI_at_Udon_by_Lilys_v1.1.unitypackage  
をインポートしてください。  
インストールは以上です。
  
## Sample Scene
HierarchyにPAPI.sceneをドラッグ&ドロップしてください。  
サンプルシーンが使用できます。  
  
## Prefab
既存のシーンにPAPIキットだけを導入したい場合は、  
Hierarchyの任意の箇所にPrefabs/PAPI.prefabをドラッグ&ドロップしてください。  

## PostProcessing
Bloomを設定してください。設定値はサンプルシーンを参考にしてください。
  
## Settings
基本値は設定済みです。以下、各値の詳細です。  

PAPI  
![001](./Images/001.png)
<dl>
<dt>L Indicators:  左側指示灯の指定</dt>
<dd>Size: 指示灯数（0またはDegrees>Sizeと同数とすること）</dd>
<dd>Element n: 指示灯オブジェクトの指定（n値はDegrees>Elementと対応）</dd>
<dt>R Indicator: 右側指示灯の設定</dt>
<dd>Size: 指示灯数（0またはDegrees>Sizeと同数とすること）</dd>
<dd>Element n: 指示灯オブジェクトの指定（n値はDegrees>Elementと対応）</dd>
<dt>Degrees: 各指示灯の傾きを設定</dt>
<dd>Size: 指示灯数</dd>
<dd>Element n: 各指示灯の傾き（十進度）指定（指示灯オブジェクト見た目傾きと赤白現示判定に使用する、傾き値。アバター頭部が指定角度より上にある時に白、下にある時に赤を現示する。n値は上記指示灯Elementのnに対応）</dd>
<dt>White: 指示灯白色現示ベース色</dt>
<dt>E White: 指示灯白色現示発光（Emission）色</dt>
<dt>Red: 指示灯赤色現示ベース色</dt>
<dt>E Red: 指示灯赤色現示発光（Emission）色</dt>
</dl>

Indicator  
![002](./Images/002.png)
<dl>
<dt>Box: 指示灯本体オブジェクト（PAPI>Degrees>Elementで指定した角度傾く）</dt>
<dt>Light: 指示灯発光オブジェクト（当該オブジェクトのMeshRendererを指定）</dt>
<dt>Scalable Min/Max: 変更の必要はありません。遠距離視認に影響します</dt>
</dl>

# バージョン情報
- v1.1 : 2020/12/13 1000m程度まで視認可能に改修
- v1.0 : 2020/12/05 初回リリース
  
# 著作権表示・ライセンス
Copyright 2020 Lily at Lily's  
  
本頒布品におけるLilyの著作物は、MITライセンスで提供しています。
